# Zaphod Acrylic Plate Case

The Zaphod acrylic plate case consists of three layers:

* Base Plate
* Switch Plate (technically two pieces, the left and right sides)
* Controller Cover

# BOM

The following BOM is required to assemble the case:

* 4x - m2 x 4mm knurled standoffs
* 4x - m2 x 8mm knurled standoffs
* 16x - m2 x 4mm screws
* 16x - m2 x 6mm screws

All screws may not be necessary, the wide variance in acrylic thickness may require either 4mm or 6mm screws.

# KiCad Project

Each layer for the case is found in a different layer in the KiCAd file:

* Base Plate - `B.SilkS`
* Switch Plate - `Eco1.User`
* Controller Cover - `F.SilkS`

To get this cut, use KiCad's "plot" function to plot those three layers out to
separate DXF files. Be sure to *uncheck* the DXF Option named "Plot graphic items using their contours" to ensure single cut lines are exported.
