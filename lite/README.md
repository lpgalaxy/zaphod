
# Zaphod Lite

![Zaphod Lite](zaphod-lite.jpg)

A hand solderable version of the original Zaphod, using any XIAO footprint controller.

If you get these produced, please consider doing a [one-time sponsorhip](https://github.com/sponsors/petejohanson/)
to support my work.

## Bill of Materials

[Interactive BoM](https://gitlab.com/lpgalaxy/zaphod/-/jobs/artifacts/main/raw/lite/zaphod_lite-ibom.html?job=exports:zaphod-lite)
gives you a high level overview, but is only partially complete. The [Zaphod Build Guide](https://docs.lpgala.xyz/docs/zaphod-build-guide/overview/) covers things like
display mounting, battery, etc. in more detail.

## Build Guide

Build guide is still in the works, until then, join the Low Profile Discord server and ask in #lpgala-works channel for help

## Schematic

![Zaphod Lite](https://gitlab.com/lpgalaxy/zaphod/-/jobs/artifacts/main/raw/lite/zaphod_lite-schematic.svg?job=exports:zaphod-lite)

## PCBs

You can get PCBs made at JLC or others by grabbing the [latest gerber zip](https://gitlab.com/lpgalaxy/zaphod/-/jobs/artifacts/main/raw/lite/JLCPCB/zaphod_lite-JLCPCB.zip?job=exports:zaphod-lite)


