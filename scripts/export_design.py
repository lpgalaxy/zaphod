#!/bin/env python3

import os
import sys
import FreeCAD

filename=sys.argv[1]

FreeCAD.open(filename)

export_file=os.path.splitext(os.path.basename(filename))[0]

if len(sys.argv) > 2:
  export_file=os.path.join(sys.argv[2], export_file)
  os.makedirs(os.path.dirname(export_file))

part = FreeCAD.ActiveDocument.ActiveObject

part.Shape.exportStep(export_file + ".step")
part.Shape.exportStl(export_file + ".stl")
